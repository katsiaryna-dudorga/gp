package by.dudorga.game_room;
import java.util.Comparator;
public class CarsSortByName implements Comparator<Toys.Car.Cars> {
    public int compare(Toys.Car.Cars o1, Toys.Car.Cars o2){ return o1.getName().compareTo(o2.getName());
    }
}

