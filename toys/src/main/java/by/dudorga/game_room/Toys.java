package by.dudorga.game_room;
import java.awt.*;
import java.util .ArrayList;
import java.util.Iterator;

public class Toys {
                //название игрушки


    static class Car extends Toys {


            public static void main(String[] args) {

                Cars s1 = new Cars("Subaru Forester", "middle", 13);
                Cars s2 = new Cars("BMW X5", "big", 20);
                Cars s3 = new Cars("Lada VAZ-2109", "little", 2);

                ArrayList<Cars> al = new ArrayList<Cars>();
                al.add(s1);
                al.add(s2);
                al.add(s3);

                Iterator itr = al.iterator();

                //traverse elements of ArrayList object
                while (itr.hasNext()) {
                    Cars st = (Cars) itr.next();
                    System.out.println(st.name + " " + st.size1 + " " + st.price+"$");
                }
            }

        static class Cars {
            String name;
            String size1;
            int price;
            Cars(  String name, String size ,int price) {
                this.name = name;
                this.size1 = size;
                this.price = price;
            }
            public double getCost() {
                return price;
            }

            public String getName() {
                return name;
            }
        }

    }

}