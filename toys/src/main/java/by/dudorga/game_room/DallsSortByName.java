package by.dudorga.game_room;
import java.util.Comparator;
public abstract class DallsSortByName implements Comparator<Dall> {
    public int compare(Dall.Dalls o1, Dall.Dalls o2)
    {return o1.getName1().compareTo(o2.getName1());
    }
}
