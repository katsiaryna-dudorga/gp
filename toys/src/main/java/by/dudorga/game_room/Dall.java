package by.dudorga.game_room;

import java.util.ArrayList;
import java.util.Iterator;

public class Dall extends Toys {
    public static void main(String[] args) {

        Dalls s1 = new Dalls("Barbie", 16);
        Dalls s2 = new Dalls("Bratz",  8);
        Dalls s3 = new Dalls("Porcelain dolls", 20);

        ArrayList<Dalls> al = new ArrayList<Dalls>();
        al.add(s1);
        al.add(s2);
        al.add(s3);

        Iterator itr = al.iterator();

        //traverse elements of ArrayList object
        while (itr.hasNext()) {
            Dalls st = (Dalls) itr.next();
            System.out.println(st.name + " " +  st.price+"$");
        }
    }

    static class Dalls {
        String name;
        int price;
        Dalls(  String name ,int price) {
            this.name = name;
            this.price = price;
        }
        public double getCost1() {
            return price;
        }

        public String getName1() {
            return name;
        }
    }
}
